# ADeswanta's GNOME Shell Extensions Samples

A collection of GNOME Shell extension samples that can help developers/creators
and make it as a bit references.

Go to [samples](/samples) to explore all samples

# Inspiration / Credits

This sample was inspired by [Just Perfection's GNOME Shell Extension Samples](https://gitlab.com/justperfection.channel/gnome-shell-extension-samples)

# License

All files have been released under The MIT License.