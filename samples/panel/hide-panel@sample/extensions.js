/**
 * ADeswanta's GNOME Shell Extensions Sample
 * 
 * @license MIT
 * @author Advendra Deswanta
 */

var method = "SIMPLE";
// They are two different methods: see README.ms for more details

const Main = imports.ui.panel;

const Clutter = imports.gi.Clutter;

function init() {}

function enable() {
    if (method == "SIMPLE")
    {
        // Hide the panel by calling hide function for panelBox
        Main.layoutManager.panelBox.hide();
        // This also work for the panel container linking to the panelBox:
        // Main.panel.hide();
    }
    else if (method == "ANIMATED")
    {
        // Make panel overlap with windows by changing trackedActor properties for panelBox
        // To make less lag or glitchy when showing back the panel
        let trackedIndex = Main.layoutManager._findActor(Main.layoutManager.panelBox);
        Main.layoutManager._trackedActors[trackedIndex].affectsStruts = false;

        // Animation duration
        let duration = 250; // 0.250 seconds
        // Animate slide out the panel using transform translation:
        Main.layoutManager.panelBox.ease({
            translation_y: Main.layoutManager.panelBox.height * -1,
            mode: Clutter.AnimationMode.EASE_OUT_QUAD,
            duration: duration,
            onComplete: () => {
                // When animation complete:
                // Hide the panel to make panel is really invisible/hided
                Main.layoutManager.panelBox.hide();
                // This also work for the panel container linking to the panelBox:
                // Main.panel.hide();
            }
        })
    }
}

function disable() {
    if (method == "SIMPLE")
    {
        // Show the panel by calling show function for panelBox
        Main.layoutManager.panelBox.show();
        // This also work for the panel container linking to the panelBox:
        // Main.panel.show();
    }
    else if (method == "ANIMATED")
    {
        // Animation duration
        let duration = 250; // 0.250 seconds
        // Animate slide in the panel using transform translation:
        Main.layoutManager.panelBox.ease({
            translation_y: 0,
            mode: Clutter.AnimationMode.EASE_OUT_QUAD,
            duration: duration,
            onComplete: () => {
                // When animation complete:

                // Restores trackedActor properties for panelBox
                let trackedIndex = Main.layoutManager._findActor(Main.layoutManager.panelBox);
                Main.layoutManager._trackedActors[trackedIndex].affectsStruts = true;

                // Hide the panel to make panel is really invisible/hided
                Main.layoutManager.panelBox.hide();
                // This also work for the panel container linking to the panelBox:
                // Main.panel.hide();

                // Hide and Show can fix windows not going under panel
                this._Main.layoutManager.panelBox.hide();
                this._Main.layoutManager.panelBox.show();
            }
        })
    }
}
