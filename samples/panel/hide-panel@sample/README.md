[Home](/) &rarr; [Samples](/samples) &rarr; [Panel](/samples/panel) &rarr; Hide Panel

# Hide Panel

**Hides the top panel**

## Information

There're two methods to hiding panel:
* Simple (SIMPLE) method:
  Hides the panel immediately
* Animated (ANIMATED) method:
  Hides the panel with slide out/in animation

## Contents

* [extensions.js](./extensions.js)
* [metadata.json](./metadata.json)

## Details

**GNOME Shell Support:** 3.36 later
<br>
**Version:** 1